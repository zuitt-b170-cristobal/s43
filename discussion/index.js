let posts = [];
let count = 1

//add post data
document.querySelector("#form-add-post").addEventListener('submit', (e) =>{
	//preventDefault allows webpage to continue executing codes w/o need to reload
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		synopsis: document.querySelector('#txt-body').value
	})

	count++
	console.log(posts);
	console.log(count);

	showPost(posts);
	alert("Movie post successfully created");
})

//show post
const showPost = (posts) =>{
	let postEntries = '';
	//forEach goes through each item one by one
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.synopsis}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

//EDIT & UPDATE
//edit
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let synopsis = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = synopsis;
}

//Update Post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	for (let i=0; i<posts.length; i++) {
		if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].synopsis = document.querySelector('#txt-edit-body').value;

			showPost(posts);
			alert('Movie post updated.')
			break
		} 
	}
})

//ACTIVITY - make delete button work
	//hint: filter method

function deletePost() {
	const element = document.querySelector("#div-post-entries");
	element.remove();
}